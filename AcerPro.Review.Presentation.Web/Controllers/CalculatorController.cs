﻿using AcerPro.Review.BusinessLayer.Calculator;
using AcerPro.Review.BusinessLayer.Calculator.DataAccess;
using AcerPro.Review.Common.Models;
using AcerPro.Review.Common.Models.Calculator;
using AcerPro.Review.Common.Models.Entities.PostEntities;
using System;
using System.Web.Mvc;
namespace AcerPro.Review.Presentation.Web.Controllers
{
    public class CalculatorController : Controller
    {
        private static CalculatorManager _manager;
        private static CalculatorDAL<CalculateModel> _calculatorDal;
        public CalculatorController()
        {
            _manager = CalculatorManager.GetInstance();
            _calculatorDal = CalculatorDAL<CalculateModel>.GetInstance();
        }
        // GET: Calculator
        public ActionResult Index()
        {
            return View(_calculatorDal.GetAll("SELECT * FROM dbo.calculated"));
        }
        [HttpPost]
        public JsonResult Save(CalculateModel parms)
        {
            try
            {
                var query = $@"INSERT INTO dbo.calculated (operation,valueone,valuetwo,result) VALUES (@Operation,@valueOne,@valueTwo,@Result)";
                var insertParam = new { Operation = parms.Operation, valueOne = parms.valueOne, valueTwo = parms.valueTwo, Result = parms.Result };
                if (_calculatorDal.Execute(query, insertParam))                
                    return Json(ServiceReturnCode.Success);                
            }
            catch (System.Exception)
            {
                return Json(Enum.GetName(typeof(ServiceReturnCode), ServiceReturnCode.Fail));
            }
            return Json(Enum.GetName(typeof(ServiceReturnCode), ServiceReturnCode.Fail));
        }
        [HttpPost]
        public JsonResult Calculate(CalculateModel parms)
        {
            var res = new ResponseMessage<CalculatorDTO>() { ReturnCode = ServiceReturnCode.Fail, Message = "Calculating failed !" };
            if (parms.valueOne > 0 && parms.valueTwo > 0)
            {
                switch (parms.Operation)
                {
                    case OperationType.Add:
                        res = _manager.Add(new CalculatorParams() { ValueOne = parms.valueOne, ValueTwo = parms.valueTwo });
                        break;
                    case OperationType.Divide:
                        res = _manager.Divide(new CalculatorParams() { ValueOne = parms.valueOne, ValueTwo = parms.valueTwo });
                        break;
                    case OperationType.Multiply:
                        res = _manager.Multiply(new CalculatorParams() { ValueOne = parms.valueOne, ValueTwo = parms.valueTwo });
                        break;
                    case OperationType.Substract:
                        res = _manager.Substract(new CalculatorParams() { ValueOne = parms.valueOne, ValueTwo = parms.valueTwo });
                        break;
                }
            }
            switch (res.ReturnCode)
            {
                case ServiceReturnCode.Success:
                    return Json(res.Data.Result);
                case ServiceReturnCode.Fail:
                    return Json(res.Message);
                default:
                    return Json(res.Message);
            }

        }
    }
}