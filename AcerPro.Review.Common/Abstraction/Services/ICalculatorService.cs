﻿using AcerPro.Review.Common.Models;
using AcerPro.Review.Common.Models.Calculator;
using System.ServiceModel;

namespace AcerPro.Review.Common.Abstraction.Services
{
    [ServiceContract]
    public interface ICalculatorService
    {
        [OperationContract]
        ResponseMessage<CalculatorDTO> Add(CalculatorParams parms);
        [OperationContract]
        ResponseMessage<CalculatorDTO> Divide(CalculatorParams parms);
        [OperationContract]
        ResponseMessage<CalculatorDTO> Multiply(CalculatorParams parms);
        [OperationContract]
        ResponseMessage<CalculatorDTO> Substract(CalculatorParams parms);
    }
}
