﻿using System.Collections.Generic;

namespace AcerPro.Review.Common.Abstraction.DataAccess
{
    public interface IRepositoryBase<T>
    {
        T Get(string query, object param = null);
        List<T> GetAll(string query, object param = null);
        bool Execute(string query, object param = null);
    }
}
