﻿using AcerPro.Review.Common.Models;
using AcerPro.Review.Common.Models.Calculator;

namespace AcerPro.Review.Common.Abstraction.Business.Calculator
{
    public interface ICalculatorManager
    {
        ResponseMessage<CalculatorDTO> Add(CalculatorParams parms);
        ResponseMessage<CalculatorDTO> Divide(CalculatorParams parms);
        ResponseMessage<CalculatorDTO> Multiply(CalculatorParams parms);
        ResponseMessage<CalculatorDTO> Substract(CalculatorParams parms);
    }
}
