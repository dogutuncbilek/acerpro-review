﻿using System;

namespace AcerPro.Review.Common.Models
{
    public class BaseEntity
    {
        public int ID { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
        public DateTime DeleteAt { get; set; }
        public bool IsActive { get; set; }
    }
}
