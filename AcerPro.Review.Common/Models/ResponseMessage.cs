﻿namespace AcerPro.Review.Common.Models
{
    public class ResponseMessage<T> where T : new()
    {
        public ServiceReturnCode ReturnCode { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
    public enum ServiceReturnCode
    {
        Success = 1,
        Fail = 2
    }
}
