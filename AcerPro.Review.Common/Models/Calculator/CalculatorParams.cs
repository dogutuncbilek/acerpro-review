﻿namespace AcerPro.Review.Common.Models.Calculator
{
    public class CalculatorParams
    {
        public int ValueOne { get; set; }
        public int ValueTwo { get; set; }
    }
}
