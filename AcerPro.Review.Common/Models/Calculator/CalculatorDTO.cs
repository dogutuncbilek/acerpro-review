﻿namespace AcerPro.Review.Common.Models.Calculator
{
    public class CalculatorDTO
    {
        public int Result { get; set; }
    }
}
