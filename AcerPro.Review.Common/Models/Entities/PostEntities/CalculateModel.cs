﻿namespace AcerPro.Review.Common.Models.Entities.PostEntities
{
    public class CalculateModel:BaseEntity
    {
        public int valueOne { get; set; }
        public int valueTwo { get; set; }
        public int Result { get; set; }
        public OperationType Operation { get; set; }
    }

    public enum OperationType
    {
        Add = 1,
        Divide = 2,
        Multiply = 3,
        Substract = 4
    }
}
