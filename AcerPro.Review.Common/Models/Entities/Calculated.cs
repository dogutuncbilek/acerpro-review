﻿namespace AcerPro.Review.Common.Models.Entities
{
    public class Calculated : BaseEntity
    {
        public string Operation { get; set; }
        public int ValueOne { get; set; }
        public int ValueTwo { get; set; }
        public int Result { get; set; }
    }
}
