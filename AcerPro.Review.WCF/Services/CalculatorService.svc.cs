﻿using AcerPro.Review.BusinessLayer.Calculator;
using AcerPro.Review.Common.Abstraction.Services;
using AcerPro.Review.Common.Models;
using AcerPro.Review.Common.Models.Calculator;
using System.ServiceModel.Activation;

namespace AcerPro.Review.WCF.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CalculatorService : ICalculatorService
    {
        private static CalculatorManager _manager;
        public CalculatorService()
        {
            if (_manager == null)
                _manager = CalculatorManager.GetInstance();
        }
        public ResponseMessage<CalculatorDTO> Add(CalculatorParams parms) => _manager.Add(parms);
        public ResponseMessage<CalculatorDTO> Divide(CalculatorParams parms) => _manager.Divide(parms);
        public ResponseMessage<CalculatorDTO> Multiply(CalculatorParams parms) => _manager.Multiply(parms);
        public ResponseMessage<CalculatorDTO> Substract(CalculatorParams parms) => _manager.Substract(parms);
    }

}
