﻿using AcerPro.Review.Common.Abstraction.DataAccess;
using AcerPro.Review.Common.Models;
using AcerPro.Review.DataAccess.Dapper;
using System;
using System.Collections.Generic;

namespace AcerPro.Review.BusinessLayer.Calculator.DataAccess
{
    public class CalculatorDAL<T> : IRepositoryBase<T> where T : BaseEntity,new()
    {
        private static object _lockObject = new object();
        private static CalculatorDAL<T> _calculatorDal;
        private static DapperRepositoryWithQuery<T> _dapperRepository;

        private CalculatorDAL()
        {
            _dapperRepository = DapperRepositoryWithQuery<T>.GetInstance();
        }
        public static CalculatorDAL<T> GetInstance()
        {
            if (_dapperRepository == null)
                lock (_lockObject)
                    _calculatorDal = new CalculatorDAL<T>();
            return _calculatorDal;
        }
        public bool Execute(string query, object param = null)
        {
            try
            {
                return _dapperRepository.Execute(query, param);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public T Get(string query, object param = null)
        {
            try
            {
                return _dapperRepository.Get(query, param);
            }
            catch (Exception ex)
            {
                return new T();
            }
        }

        public List<T> GetAll(string query, object param = null)
        {
            try
            {
                return _dapperRepository.GetAll(query, param);
            }
            catch (Exception ex)
            {
                return new List<T>();
            }
        }
    }
}
