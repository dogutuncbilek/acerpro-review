﻿using AcerPro.Review.Common.Abstraction.Business.Calculator;
using AcerPro.Review.Common.Models;
using AcerPro.Review.Common.Models.Calculator;
using System;

namespace AcerPro.Review.BusinessLayer.Calculator
{
    public class CalculatorManager : ICalculatorManager
    {
        // maybe we use the mapper tool // 30.12.2021 03.28
        #region Definitons

        private static object _lockObject = new object();
        private static CalculatorManager _calculatorManager;
        private static CalculatorService.Calculator _calculatorService;
        private CalculatorManager()
        {
            _calculatorService = new CalculatorService.Calculator();
        }
        public static CalculatorManager GetInstance()
        {
            if (_calculatorManager == null)
                lock (_lockObject)
                    _calculatorManager = new CalculatorManager();
            return _calculatorManager;
        }

        #endregion

        #region Add

        /// <summary>
        /// Add Process
        /// </summary>
        /// <param name="parms"></param>
        /// <returns>ResponseMessage<CalculatorDTO></returns>
        public ResponseMessage<CalculatorDTO> Add(CalculatorParams parms)
        {
            var res = new ResponseMessage<CalculatorDTO>();
            try
            {
                var data = new CalculatorDTO();
                data.Result = _calculatorService.Add(parms.ValueOne, parms.ValueTwo);
                res.Data = data;
                res.ReturnCode = ServiceReturnCode.Success;
                res.Message = Enum.GetName(typeof(ServiceReturnCode), ServiceReturnCode.Success);
            }
            catch (Exception ex)
            {
                res.ReturnCode = ServiceReturnCode.Fail;
                res.Message = ex.ToString();
            }
            return res;
        }

        #endregion

        #region Divide

        /// <summary>
        /// Divide Process
        /// </summary>
        /// <param name="parms"></param>
        /// <returns>ResponseMessage<CalculatorDTO></returns>
        public ResponseMessage<CalculatorDTO> Divide(CalculatorParams parms)
        {

            var res = new ResponseMessage<CalculatorDTO>();
            try
            {
                var data = new CalculatorDTO();
                data.Result = _calculatorService.Divide(parms.ValueOne, parms.ValueTwo);
                res.Data = data;
                res.ReturnCode = ServiceReturnCode.Success;
                res.Message = Enum.GetName(typeof(ServiceReturnCode), ServiceReturnCode.Success);
            }
            catch (Exception ex)
            {
                res.ReturnCode = ServiceReturnCode.Fail;
                res.Message = ex.ToString();
            }
            return res;
        }

        #endregion

        #region Multiply

        /// <summary>
        /// Multiply Process
        /// </summary>
        /// <param name="parms"></param>
        /// <returns>ResponseMessage<CalculatorDTO></returns>
        public ResponseMessage<CalculatorDTO> Multiply(CalculatorParams parms)
        {

            var res = new ResponseMessage<CalculatorDTO>();
            try
            {
                var data = new CalculatorDTO();
                data.Result = _calculatorService.Multiply(parms.ValueOne, parms.ValueTwo);
                res.Data = data;
                res.ReturnCode = ServiceReturnCode.Success;
                res.Message = Enum.GetName(typeof(ServiceReturnCode), ServiceReturnCode.Success);
            }
            catch (Exception ex)
            {
                res.ReturnCode = ServiceReturnCode.Fail;
                res.Message = ex.ToString();
            }
            return res;
        }

        #endregion

        #region Substract

        /// <summary>
        /// Substract Process
        /// </summary>
        /// <param name="parms"></param>
        /// <returns>ResponseMessage<CalculatorDTO></returns>
        public ResponseMessage<CalculatorDTO> Substract(CalculatorParams parms)
        {

            var res = new ResponseMessage<CalculatorDTO>();
            try
            {
                var data = new CalculatorDTO();
                data.Result = _calculatorService.Subtract(parms.ValueOne, parms.ValueTwo);
                res.Data = data;
                res.ReturnCode = ServiceReturnCode.Success;
                res.Message = Enum.GetName(typeof(ServiceReturnCode), ServiceReturnCode.Success);
            }
            catch (Exception ex)
            {
                res.ReturnCode = ServiceReturnCode.Fail;
                res.Message = ex.ToString();
            }
            return res;
        }

        #endregion
    }
}
