﻿using AcerPro.Review.BusinessLayer.Calculator;
using AcerPro.Review.Common.Models.Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcerPro.Review.Tests
{
    [TestClass]
    public class CalculatorTests
    {
        [TestMethod]
        public void Calculator_Manager_Add()
        {
            var manager = CalculatorManager.GetInstance();
            var parms = new CalculatorParams() { ValueOne = 10, ValueTwo = 20 };
            var res = parms.ValueOne + parms.ValueTwo;
            var managerRes = manager.Add(parms);
            Assert.AreEqual(managerRes, res);
        }
        [TestMethod]
        public void Calculator_Manager_Divide()
        {
            var manager = CalculatorManager.GetInstance();
            var parms = new CalculatorParams() { ValueOne = 10, ValueTwo = 20 };
            var res = parms.ValueOne + parms.ValueTwo;
            var managerRes = manager.Divide(parms);
            Assert.AreEqual(managerRes, res);
        }
        [TestMethod]
        public void Calculator_Manager_Multiply()
        {
            var manager = CalculatorManager.GetInstance();
            var parms = new CalculatorParams() { ValueOne = 10, ValueTwo = 20 };
            var res = parms.ValueOne + parms.ValueTwo;
            var managerRes = manager.Multiply(parms);
            Assert.AreEqual(managerRes, res);
        }
        [TestMethod]
        public void Calculator_Manager_Substract()
        {
            var manager = CalculatorManager.GetInstance();
            var parms = new CalculatorParams() { ValueOne = 10, ValueTwo = 20 };
            var res = parms.ValueOne + parms.ValueTwo;
            var managerRes = manager.Substract(parms);
            Assert.AreEqual(managerRes, res);
        }
    }
}
