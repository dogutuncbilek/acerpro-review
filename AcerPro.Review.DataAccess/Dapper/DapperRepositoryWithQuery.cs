﻿using AcerPro.Review.Common.Abstraction.DataAccess;
using AcerPro.Review.Common.Models;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace AcerPro.Review.DataAccess.Dapper
{
    public class DapperRepositoryWithQuery<T> : IRepositoryBase<T> where T : BaseEntity
    {
        private static object _lockObject = new object();
        private static DapperRepositoryWithQuery<T> _dapperRepository;
        private static IDbConnection _con { get => new SqlConnection(@"server=.\sql2012;database=acerproreview;integrated security=true"); }

        private DapperRepositoryWithQuery()
        {

        }
        public static DapperRepositoryWithQuery<T> GetInstance()
        {
            if (_dapperRepository == null)
                lock (_lockObject)
                    _dapperRepository = new DapperRepositoryWithQuery<T>();
            return _dapperRepository;
        }
        public bool Execute(string query, object param = null)
        {
            using (var dapperContext = _con)
                if (param == null)
                    return dapperContext.Execute(query) > 0;
                else
                    return dapperContext.Execute(query, param) > 0;
        }

        public T Get(string query, object param = null)
        {
            using (var dapperContext = _con)
                if (param == null)
                    return dapperContext.QueryFirstOrDefault<T>(query);
                else
                    return dapperContext.QueryFirstOrDefault<T>(query, param);
        }

        public List<T> GetAll(string query, object param = null)
        {
            using (var dapperContext = _con)
                if (param == null)
                    return dapperContext.Query<T>(query).AsList();
                else
                    return dapperContext.Query<T>(query, param).AsList();

        }
    }
}
