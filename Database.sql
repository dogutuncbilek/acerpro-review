USE [master]
GO

/****** Object:  Database [acerproreview]    Script Date: 6.01.2022 13:08:21 ******/
CREATE DATABASE [acerproreview]

ALTER DATABASE [acerproreview] SET COMPATIBILITY_LEVEL = 110
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [acerproreview].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [acerproreview] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [acerproreview] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [acerproreview] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [acerproreview] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [acerproreview] SET ARITHABORT OFF 
GO

ALTER DATABASE [acerproreview] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [acerproreview] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [acerproreview] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [acerproreview] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [acerproreview] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [acerproreview] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [acerproreview] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [acerproreview] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [acerproreview] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [acerproreview] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [acerproreview] SET  DISABLE_BROKER 
GO

ALTER DATABASE [acerproreview] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [acerproreview] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [acerproreview] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [acerproreview] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [acerproreview] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [acerproreview] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [acerproreview] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [acerproreview] SET RECOVERY FULL 
GO

ALTER DATABASE [acerproreview] SET  MULTI_USER 
GO

ALTER DATABASE [acerproreview] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [acerproreview] SET DB_CHAINING OFF 
GO

ALTER DATABASE [acerproreview] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [acerproreview] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [acerproreview] SET  READ_WRITE 
GO


USE [acerproreview]
GO

/****** Object:  Table [dbo].[Calculated]    Script Date: 6.01.2022 13:09:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Calculated](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Operation] [nvarchar](50) NULL,
	[ValueOne] [int] NULL,
	[ValueTwo] [int] NULL,
	[Result] [int] NULL,
	[CreateAt] [datetime] NULL,
	[UpdateAt] [datetime] NULL,
	[DeleteAt] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Calculated] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Calculated] ADD  CONSTRAINT [DF_Calculated_CreateAt]  DEFAULT (getdate()) FOR [CreateAt]
GO

ALTER TABLE [dbo].[Calculated] ADD  CONSTRAINT [DF_Calculated_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO